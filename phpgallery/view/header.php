<div class="ui floating message">
    <div class="header">
        <form action="" class="ui form">
            <div class="three fields">
                <div class="four wide field">
                    <a href="/phpgallery/index.php" class="logo">
                        <img src="/phpgallery/src/images/logo1.png" alt="logo" align="left">
                    </a>
                </div>
                <div class="eight wide field">
                    <div align="center">
                        <div>
                            some text
                        </div>
                    </div>
                </div>
                <?php
                if (isset($_SESSION['username'])){
                    echo '<div class="five wide field" id="loggedIn">';
                    echo '<div align="right">';
                    echo 'Welcome ';
                    ?>
                    <div class="ui dropdown">
                        <div class="text">
                            <?php echo $_SESSION['username']; ?>
                        </div>
                        <i class="dropdown icon left"></i>
                        <div class="menu headerProfileMenu">
                            <div class="item" onClick="window.location.href='/phpgallery/model/changePersonalData.php'">
                                <a href="/phpgallery/model/changePersonalData.php"class="headerProfileMenuItem" style="color: rgba(0,0,0,.87)">Редактировать личные данные</a>
                            </div>
                            <div class="item" onClick="window.location.href='/phpgallery//model/personalGallery.php'">
                                <a href="/phpgallery//model/personalGallery.php"class="headerProfileMenuItem" style="color: rgba(0,0,0,.87)">Личная галерея</a>
                            </div>
                            <div class="item" onClick="window.location.href='/phpgallery/model/logOut.php'">
                                <a href="/phpgallery/model/logOut.php"class="headerProfileMenuItem" style="color: rgba(0,0,0,.87)">Выйти</a>
                            </div>
                        </div>
                    </div>
                <?php


                    echo '</div>';
                    echo '</div>';
                    ?>



                <?php
                } else {
                    echo '<div class="five wide field" id="unLoggedIn">';
                    echo '<div align="right">';
                    echo 'Please ';
                    echo '<a href="/phpgallery/model/login.php" id="loggIn">log in</a>';
                    echo ' or ';
                    echo '<a href="/phpgallery/model/registration.php" id="register">register</a>';
                    echo '</div>';
                    echo '</div>';
                }


                ?>
            </div>
        </form>
    </div>
</div>