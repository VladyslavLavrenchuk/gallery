<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="src/Semantic-UI-CSS-master/semantic.min.css">
    <link rel="stylesheet" href="view/main.css">
    <meta charset="utf-8">
    <title>gallery</title>
</head>
<body>
<?php
require_once 'model/connection.php';
?>
<div class="ui equal width center aligned padded grid">
    <div class="row">
        <div class="sixteen wide column">
            <div class="ui container">
                <?php
                require_once 'view/header.php';
                ?>
                <div class="slider">
                    <div id="block-for-slider">
                        <div id="viewport" style="height: 450px">
                            <ul id="slidewrapper">
                                <li class="slide"><img src="https://hsto.org/files/8d4/b19/80d/8d4b1980d48c418090e2c4466d8c06e1.jpg" alt="1" class="slide-img"></li>
                                <li class="slide"><img src="https://hsto.org/files/ef1/3d7/97e/ef13d797e4c642c7a1d4b2b91f7ad7b3.jpg" alt="2" class="slide-img"></li>
                                <li class="slide"><img src="https://hsto.org/files/ec5/592/f1e/ec5592f1e814401eb38305682a8e88d4.jpg" alt="3" class="slide-img"></li>
                                <li class="slide"><img src="https://hsto.org/files/eda/61a/3c5/eda61a3c53db408d820643998d9acd81.jpg" alt="4" class="slide-img"></li>
                            </ul>

                            <div id="prev-next-btns">
                                <div id="prev-btn">
                                    <i class="arrow alternate circle left big outline inverted icon"></i>
                                </div>
                                <div id="next-btn">
                                    <i class="arrow alternate circle right big outline inverted icon"></i>
                                </div>
                            </div>

                            <ul id="nav-btns">
                                <li class="slide-nav-btn"></li>
                                <li class="slide-nav-btn"></li>
                                <li class="slide-nav-btn"></li>
                                <li class="slide-nav-btn"></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="navBar">
                    <form action="" method="GET" name="categoryForm" id="categoryForm">
                        <select class="ui fluid dropdown large" name="categorySelect" id="categorySelect">
                            <option selected="selected" value="0">Select category</option>
                            <option value="1">Flowers</option>
                            <option value="2">Food</option>
                            <option value="3">Sport</option>
                            <option value="4">Cars</option>
                            <option value="5">Ocean</option>
                            <option value="6">Animals</option>
                            <option value="7">Games</option>
                            <option value="8">All</option>
                        </select>
                    </form>
                </div>
                <div class="mainContent">
                    <?php
                    $category = $_GET['categorySelect'];
                    if (isset($_GET['page'])) {
                        $page = $_GET['page'];
                    } else {
                        $page = 1;
                    }
                    $no_of_records_per_page = 8;
                    $offset = ($page-1) * $no_of_records_per_page;

                    $total_pages_sql = "SELECT COUNT(*) FROM pictures";
                    $result = mysqli_query($link,$total_pages_sql);
                    $total_rows = mysqli_fetch_array($result)[0];
                    $total_pages = ceil($total_rows / $no_of_records_per_page);
                    //
                    if ($category == false || $category == 0 || $category == 8){
                        $query = "SELECT * FROM pictures LEFT JOIN pictures_cat ON pictures.id = pictures_cat.picture_id
                                              LEFT JOIN category ON category.id = pictures_cat.category_id
                                              LEFT JOIN users ON users.id = pictures.user_id LIMIT $offset, $no_of_records_per_page";
                        $result = mysqli_query($link, $query);
                    } else{
                        $total_pages_sql = "SELECT COUNT(*) FROM pictures LEFT JOIN pictures_cat ON pictures.id = pictures_cat.picture_id
                                              LEFT JOIN category ON category.id = pictures_cat.category_id
                                              LEFT JOIN users ON users.id = pictures.user_id WHERE category_id = $category";
                        $result = mysqli_query($link,$total_pages_sql);
                        $total_rows = mysqli_fetch_array($result)[0];
                        $total_pages = ceil($total_rows / $no_of_records_per_page);

                        $query = "SELECT * FROM pictures LEFT JOIN pictures_cat ON pictures.id = pictures_cat.picture_id
                                              LEFT JOIN category ON category.id = pictures_cat.category_id
                                              LEFT JOIN users ON users.id = pictures.user_id WHERE category_id = $category LIMIT $offset, $no_of_records_per_page";
                        $result = mysqli_query($link, $query);
                    }
                    echo'<div class="fields">';
                    echo "<div class=\"ui four link cards left aligned\">";
                    if(mysqli_num_rows($result)>0){
                        while ($row = mysqli_fetch_array($result)){
                            echo "<div class=\"ui card myGalleryItem\">";
                            echo "<div class=\"image\">";
                            echo "<img class='galleryItemImg' src='src/uploads/".$row['real_name']."'/>";
                            echo "</div>";
                            ?>
                            <?php
                            ?>
                            <table>
                                <tr>
                                    <td>
                                        <a class="iconsLinkIndex" href="<?php echo 'src/uploads/'.$row['real_name']?>" data-title="Скачать" download><i class="cloud download icon iconColor"></i></a>
                                    </td>
                                    <td>
                                        <a class="iconsLinkIndex" href="<?php echo 'src/uploads/'.$row['real_name']?>" data-title="Открыть в полном размере"><i class="expand icon iconColor"></i></a>
                                    </td>
                                </tr>
                            </table>
                            <?php
                            echo "</div>";
                        }
                    }

                    echo'</div>';
                    echo '</div>';
                    mysqli_free_result($result);
                    //

                    ?>
                    <div class="ui pagination menu center" style="margin-top: 30px">
                        <a class="item" href="<?php echo "?categorySelect=".$category. '&page=1'?>">First</a>
                        <a class="item" href="<?php if($page <= 1){ echo '#'; } else { echo "?categorySelect=".$category."&page=".($page - 1); } ?>">Prev</a>
                        <a class="item" href="<?php if($page >= $total_pages){ echo '#'; } else { echo "?categorySelect=".$category."&page=".($page + 1); } ?>">Next</a>
                        <a class="item" href="<?php echo"?categorySelect=".$category. '&page='.$total_pages; ?>">Last</a>
                    </div>


                    <?php
                    require_once 'view/footer.php';
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="src/jquery-3.3.1.min.js"></script>
<script src="src/Semantic-UI-CSS-master/semantic.min.js"></script>
<script src="view/script.js"></script>
</body>
</html>