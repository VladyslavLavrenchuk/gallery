<?php
session_start();
require_once 'auth.php'
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="/phpgallery/src/Semantic-UI-CSS-master/semantic.min.css">
    <link rel="stylesheet" href="/phpgallery/view/main.css">
    <meta charset="utf-8">
    <title>gallery</title>
</head>
<body>
<div class="ui grid">
    <div class="row ">
        <div class="sixteen wide column">
            <div class="ui container">
                <div class="fields">
                <?php
                require_once '../view/header.php';
                ?>
                <?php
                require_once ('connection.php');
                $id = $_SESSION['id'];
                $query = "SELECT * FROM users  WHERE id = '$id'";
                $result = mysqli_query($link, $query);
                $row = mysqli_fetch_array($result);
                ?>
                        <form class="ui form">
                            <div class="fields">
                                <div class="five wide field">
                                    <div class="ui message">
                                        <p>
                                            Здесь указаны ваши персональные данные:</br>
                                            имя пользователя, имя, фамилия.</br>
                                            Обновляйте их по мере необходимости.</br>
                                            Чтобы обновить необходимую информацию - нажмите
                                            <i class="angle right icon"></i>
                                        </p>
                                    </div>

                                </div>
                                <div class="eleven wide field">
                                    <table class="ui selectable table center">
                                        <tbody>
                                        <tr>
                                            <td>Имя учетной записи</td>
                                            <td><?php echo $row['username']?></td>
                                            <td>
                                                <a href="changePersonalDataUsername.php">
                                                    <i class="angle right icon" style="color: rgba(0,0,0,.87);"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Ваше имя</td>
                                            <td><?php echo $row['firstname']?></td>
                                            <td>
                                                <a href="changePersonalDataFirstname.php">
                                                    <i class="angle right icon" style="color: rgba(0,0,0,.87);"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Ваша фамилия</td>
                                            <td><?php echo $row['lastname']?></td>
                                            <td>
                                                <a href="changePersonalDataLastname.php">
                                                    <i class="angle right icon" style="color: rgba(0,0,0,.87);"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </form>

                    </div>

                <?php
                require_once '../view/footer.php';
                ?>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
<script src="/phpgallery/src/jquery-3.3.1.min.js"></script>
<script src="/phpgallery/src/Semantic-UI-CSS-master/semantic.min.js"></script>
<script src="/phpgallery/view/script.js"></script>

