<?php
session_start();
require_once ('connection.php');
require_once 'upload.php';

$filesize = $_FILES['userfile']['size'];
$filetype = $_FILES['userfile']['type'];
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="/phpgallery/src/Semantic-UI-CSS-master/semantic.min.css">
    <link rel="stylesheet" href="/phpgallery/view/main.css">
    <meta charset="utf-8">
    <title>gallery</title>
</head>
<body>
<div class="ui equal width center aligned padded grid">
    <div class="row ">
        <div class="sixteen wide column">
            <div class="ui container">
                <div class="fields">


                   
<?php
require_once '../view/header.php';
if(!empty($_FILES)){
    if ($filesize <10000000){
        if ($filetype == 'image/png'|| $filetype == 'image/jpeg'|| $filetype == 'image/bmp'){
            $upload = upload();

        }else {
            $upload = false;
            echo '<div class="ui red message compact">';
            echo '<div class="header">Неверный формат файла, разрешены только png, jpeg и bmp</div>';
            echo '<p align="center"><a class="ui button red" href="personalGallery.php" style="margin-top: 25px;">Ok</a></p>';
            echo '</div>';

//            header('Location: '.'personalGallery.php');
        }

    }else{
        $upload = false;
        echo '<div class="ui red message compact">';
        echo '<div class="header">Слишком большой файл</div>';
        echo '<p align="center"><a class="ui button red" href="personalGallery.php" style="margin-top: 25px;">Ok</a></p>';
        echo '</div>';
//        header('Location: '.'personalGallery.php');
    }

}else{
    $upload = false;
    echo '<div class="ui red message">';
    echo '<div class="header">Выберите файл</div>';
    echo '<p><a href="personalGallery.php">back</a></p>';
    echo '</div>';
//    header('Location: '.'personalGallery.php');
}

$title = htmlentities(mysqli_real_escape_string($link, $_POST['name']));
$category = htmlentities(mysqli_real_escape_string($link, $_POST['categorySelect']));

if ($title == ""){
    $title = $upload['name'];
}

if ($upload){

    $path = $upload['real_name'];
    $date = date("Y-m-d H:i:s");
    $id = $_SESSION['id'];


    $query ="INSERT INTO pictures VALUES(NULL, '$id','$title','$date','$path')";
    $result = mysqli_query($link, $query);

    $query = "SELECT * FROM pictures ORDER by id DESC LIMIT 1";
    $result = mysqli_query($link, $query);
    $row = mysqli_fetch_array($result);
    $pictureID = $row['id'];

    $query ="INSERT INTO pictures_cat VALUES('$pictureID','$category')";
    $result = mysqli_query($link, $query);

    // закрываем подключение
    mysqli_close($link);

    header('Location: '.'personalGallery.php');
};

?>

<?php
require_once '../view/footer.php';
?>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
<script src="/phpgallery/src/jquery-3.3.1.min.js"></script>
<script src="/phpgallery/src/Semantic-UI-CSS-master/semantic.min.js"></script>
<script src="/phpgallery/view/script.js"></script>
</html>
