<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="/phpgallery/src/Semantic-UI-CSS-master/semantic.min.css">
    <link rel="stylesheet" href="/phpgallery/view/main.css">
    <meta charset="utf-8">
    <title>gallery</title>
    <script src="../ehr/script.js"></script>
</head>
<body>
<div class="ui equal width center aligned padded grid">
    <div class="row ">
        <div class="sixteen wide column">
            <div class="ui container">
                <div class="fields">
                <?php
                require_once '../view/header.php';
                ?>
                    <h4 class="ui horizontal divider">Enter your login and password</h4>
                <?php
                require_once ('connection.php');

                if(isset($_POST['submit'])){

                    if (empty($_POST['login']) && !empty($_POST['password'])){
                        echo '<div class="ui error message">';
                        echo '<div>Введите логин</div>';
                        echo '</div>';
                    }
                    if (!empty($_POST['login']) && empty($_POST['password'])){
                        echo '<div class="ui error message">';
                        echo '<div>Введите Пароль</div>';
                        echo '</div>';
                    }
                    if (empty($_POST['login']) && empty($_POST['password'])){
                        echo '<div class="ui error message">';
                        echo '<div>Введите логин и пароль</div>';
                        echo '</div>';
                    }



                } $a=1;

                if (!empty($_POST['login']) and !empty($_POST['password'])){
                    $login = $_POST['login'];
                    $password = $_POST['password'];
                    $passwordmd5 = md5($password);
                    $query = 'SELECT * FROM users WHERE username="'.$login.'" AND password="'.$passwordmd5.'"';
                    $result= mysqli_query($link,$query);
                    $user = mysqli_fetch_assoc($result);

                    if (!empty($user)){
                        $_SESSION['auth'] = true;
                        $_SESSION['id'] = $user['id'];
                        $_SESSION['username'] = $user['username'];
                        $_SESSION['email'] = $user['email'];
                        $_SESSION['firstname'] = $user['firstname'];
                        $_SESSION['lastname'] = $user['lastname'];
                        echo 'Hello, '.$_SESSION['firstname'].' '.$_SESSION['lastname'];
                        header('Location: '.'/phpgallery/index.php');
                        exit;

                    }else{
                        echo '<div class="ui error message">';
                        echo '<div>Неверный логин или пароль</div>';
                        echo '</div>';

                    }
                }



                ?>
                <div class="logInForm" id="logInForm">
                    <form action="" method="POST" class="ui form">
                        <div class="fields">
                            <div class="six wide field"></div>
                            <div class="four wide field">
                                <label>Username</label>
                                <input name="login" type="text" placeholder="Username">
                            </div>
                            <div class="six wide field"></div>
                        </div>
                        <div class="fields">
                            <div class="six wide field"></div>
                            <div class="four wide field">
                                <label>Password</label>
                                <input name="password" type="password">
                                <div class="six wide field"></div>
                            </div>
                        </div>
                        <div class="fields">
                            <div class="sixteen wide field">
                                <div align="center">
                                    <button type="submit" name="submit" value="submit" class="positive ui button">Login</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <?php
                require_once '../view/footer.php';
                ?>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="/phpgallery/src/jquery-3.3.1.min.js"></script>
<script src="/phpgallery/src/Semantic-UI-CSS-master/semantic.min.js"></script>
<script src="/phpgallery/view/script.js"></script>
</body>
</html>

