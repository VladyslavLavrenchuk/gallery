<?php
session_start();
require_once 'auth.php'
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="/phpgallery/src/Semantic-UI-CSS-master/semantic.min.css">
    <link rel="stylesheet" href="/phpgallery/view/main.css">
    <meta charset="utf-8">
    <title>gallery</title>
</head>
<body>
<div class="ui grid">
    <div class="row ">
        <div class="sixteen wide column">
            <div class="ui container">
                <?php
                require_once '../view/header.php';
                ?>
                <div class="fields">
                    Добро пожаловать в ваш личный кабинет.
                </div>
                <div class="fields">
                    <div class="field">
                        <a href="../model/changePersonalData.php">Редактировать личные данные</a>
                    </div>
                    <div class="field">

                    </div>
                </div>
                <?php
                require_once '../view/footer.php';
                ?>
            </div>
        </div>
    </div>
</div>
</body>
</html>
<script src="/phpgallery/src/jquery-3.3.1.min.js"></script>
<script src="/phpgallery/src/Semantic-UI-CSS-master/semantic.min.js"></script>
<script src="/phpgallery/view/script.js"></script>

