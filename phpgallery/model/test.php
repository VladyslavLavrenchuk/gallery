<?php
session_start();
require_once 'auth.php'
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="/phpgallery/src/Semantic-UI-CSS-master/semantic.min.css">
    <link rel="stylesheet" href="/phpgallery/view/main.css">
    <meta charset="utf-8">
    <title>gallery</title>
</head>
<body>
<div class="ui grid">
    <div class="row ">
        <div class="sixteen wide column">
            <div class="ui container">
                <?php
                require_once '../view/header.php';
                ?>
                <?php
                require_once ('connection.php');
                $id = $_SESSION['id'];
                $username = htmlentities(mysqli_real_escape_string($link, $_POST['username']));
                $email = htmlentities(mysqli_real_escape_string($link, $_POST['email']));
                $firstname = htmlentities(mysqli_real_escape_string($link, $_POST['firstname']));
                $lastname = htmlentities(mysqli_real_escape_string($link, $_POST['lastname']));
                $password = htmlentities(mysqli_real_escape_string($link, $_POST['password']));
                $password2 = htmlentities(mysqli_real_escape_string($link, $_POST['confirm_password']));
                $passwordmd5 = md5($password);
                $query = "SELECT * FROM users  WHERE id = '$id'";
                $result = mysqli_query($link, $query);
                $row = mysqli_fetch_array($result);
                $thisPassword = $row['password'];
                $thisPasswordmd5 = md5($thisPassword);



                if(isset($_POST['submit'])){

                    if (empty($_POST['password'])){
                        echo '<div class="ui error message">';
                        echo '<div>Введите пароль</div>';
                        echo '</div>';
                    }
                    if (empty($_POST['confirm_password'])){
                        echo '<div class="ui error message">';
                        echo '<div>Введите подтверждение пароля</div>';
                        echo '</div>';

                    }
                    if ((isset($_POST['password'])) && (isset($_POST['confirm_password']) && $_POST['password'] !== $_POST['confirm_password'])){
                        echo '<div class="ui error message">';
                        echo '<div>Пароли должны совпадать</div>';
                        echo '</div>';
                    }
                    echo '</div>';
                }



                if (!empty($_POST['username']) && $thisPassword == $passwordmd5 && $_POST['password']== $_POST['confirm_password']){
                    $query = "UPDATE users SET username='$username' WHERE id = '$id'";
                    $result = mysqli_query($link, $query);
                    echo'<div class="ui info message">';
                    echo'<div>Имя пользователя успешно изменено</div>';
                    echo'</div>';
                }else
//                    echo 'Неверный пароль';
                    if (!empty($_POST['email']) && $thisPassword == $passwordmd5 && $_POST['password']== $_POST['confirm_password']){
                        $query = "UPDATE users SET email='$email' WHERE id = '$id'";
                        $result = mysqli_query($link, $query);
                        echo'<div class="ui info message">';
                        echo'<div>Email успешно изменен</div>';
                        echo'</div>';
                    }else
//                    echo 'Неверный пароль';
                        if (!empty($_POST['firstname']) && $thisPassword == $passwordmd5 && $_POST['password']== $_POST['confirm_password']){
                            $query = "UPDATE users SET firstname='$firstname' WHERE id = '$id'";
                            $result = mysqli_query($link, $query);
                            echo'<div class="ui info message">';
                            echo'<div>Имя Успешно изменено</div>';
                            echo'</div>';
                        }else
//                    echo 'Неверный пароль';
                            if (!empty($_POST['lastname']) && $thisPassword == $passwordmd5 && $_POST['password']== $_POST['confirm_password']){
                                $query = "UPDATE users SET lastname='$lastname' WHERE id = '$id'";
                                $result = mysqli_query($link, $query);
                                echo'<div class="ui info message">';
                                echo'<div>Фамилия успешно изменена</div>';
                                echo'</div>';
                            }else
//                    echo 'Неверный пароль';
                                if (!empty($_POST['password']) && $thisPassword == $passwordmd5 && $_POST['password']== $_POST['confirm_password']){
                                    $query = "UPDATE users SET password='$password' WHERE id = '$id'";
                                    $result = mysqli_query($link, $query);
                                    echo'<div class="ui info message">';
                                    echo'<div>email успешно изменен</div>';
                                    echo'</div>';
                                }else
//




                                    ?>

                                    <form action="" class="ui form" method="POST">
                                        <div class="fields">
                                            <div class="six wide field"></div>
                                            <div class="four wide field">
                                                <label>Username</label>
                                                <input name="username" type="text" placeholder="username">
                                            </div>
                                            <div class="six wide field"></div>
                                        </div>
                                        <div class="fields">
                                            <div class="six wide field"></div>
                                            <div class="four wide field">
                                                <label>e-mail</label>
                                                <input name="email" type="text" placeholder="e-mail">
                                                <div class="six wide field"></div>
                                            </div>
                                        </div>
                                        <div class="fields">
                                            <div class="six wide field"></div>
                                            <div class="four wide field">
                                                <label>First name</label>
                                                <input name="firstname" type="text" placeholder="First name">
                                                <div class="six wide field"></div>
                                            </div>
                                        </div>
                                        <div class="fields">
                                            <div class="six wide field"></div>
                                            <div class="four wide field">
                                                <label>Last name</label>
                                                <input name="lastname" type="text" placeholder="Last name">
                                                <div class="six wide field"></div>
                                            </div>
                                        </div>
                                        <div class="fields">
                                            <div class="six wide field"></div>
                                            <div class="four wide field">
                                                <label>Password</label>
                                                <input name="password" type="password" placeholder="password">
                                                <div class="six wide field"></div>
                                            </div>
                                        </div>
                                        <div class="fields">
                                            <div class="six wide field"></div>
                                            <div class="four wide field">
                                                <label>Confirm password</label>
                                                <input name="confirm_password" type="password" placeholder="confirm password">
                                                <div class="six wide field"></div>
                                            </div>
                                        </div>
                                        <div class="fields">
                                            <div class="sixteen wide field">
                                                <div align="center">
                                                    <button type="submit" name="submit" value="submit" class="positive ui button">Apply changes</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <?php
                require_once '../view/footer.php';
                ?>
            </div>
        </div>
    </div>
</div>
</body>
</html>
<script src="/phpgallery/src/jquery-3.3.1.min.js"></script>
<script src="/phpgallery/src/Semantic-UI-CSS-master/semantic.min.js"></script>
<script src="/phpgallery/view/script.js"></script>

