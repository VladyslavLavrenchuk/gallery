<?php
session_start();
session_destroy();
?>
</div>
<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="/phpgallery/src/Semantic-UI-CSS-master/semantic.min.css">
    <link rel="stylesheet" href="/phpgallery/view/main.css">
    <meta charset="utf-8">
    <title>gallery</title>
    <script src="../ehr/script.js"></script>
</head>
<body>
<div class="ui grid">
    <div class="row ">
        <div class="sixteen wide column">
            <div class="ui container">
                <?php
                require_once '../view/header.php';
                ?>
                <div >
                    <form action="" class="ui form">
                        <div class="fields">
                            <div class="six wide field"></div>
                            <div class="four wide field">
                                <h4>
                                    You have successfully updated your information
                                </h4>
                                <a href="/phpgallery/index.php">Back to login page</a>
                            </div>
                        </div>
                    </form>
                </div>
                <?php
                require_once '../view/footer.php';
                ?>
            </div>
        </div>
    </div>

    <script src="/phpgallery/src/jquery-3.3.1.min.js"></script>
    <script src="/phpgallery/src/Semantic-UI-CSS-master/semantic.min.js"></script>
    <script src="/phpgallery/view/script.js"></script>
</body>
</html>