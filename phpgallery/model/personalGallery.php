<?php
session_start();
//require_once 'auth.php'
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="/phpgallery/src/Semantic-UI-CSS-master/semantic.min.css">
    <link rel="stylesheet" href="/phpgallery/view/main.css">
    <meta charset="utf-8">
    <title>gallery</title>
</head>
<body>
<?php
require_once '../model/connection.php';
?>

<div class="ui grid">
    <div class="row ">
        <div class="sixteen wide column">
            <div class="ui container">
                <?php
                require_once '../view/header.php';
                ?>
                <div class="ui top attached tabular menu">
                    <a class="item active" data-tab="first">Ваша галерея</a>
                    <a class="item" data-tab="second">Добавить изображение</a>
                </div>
                <div class="ui bottom attached active tab segment" data-tab="first">
                    <form action="" method="GET" name="categoryForm" id="categoryForm" style="margin-bottom: 20px">
                        <select class="ui fluid dropdown large" name="categorySelect" id="categorySelect">
                            <option selected="selected" value="0">Select category</option>
                            <option value="1">Flowers</option>
                            <option value="2">Food</option>
                            <option value="3">Sport</option>
                            <option value="4">Cars</option>
                            <option value="5">Ocean</option>
                            <option value="6">Animals</option>
                            <option value="7">Games</option>
                            <option value="8">All</option>
                        </select>
                    </form>
                    <?php
                    $userId = $_SESSION['id'];
                    $category = $_GET['categorySelect'];
                    if (isset($_GET['page'])) {
                        $page = $_GET['page'];
                    } else {
                        $page = 1;
                    }
                    $no_of_records_per_page = 6;
                    $offset = ($page-1) * $no_of_records_per_page;

                    $total_pages_sql = "SELECT COUNT(*) FROM pictures WHERE user_id = $userId";
                    $result = mysqli_query($link,$total_pages_sql);
                    $total_rows = mysqli_fetch_array($result)[0];
                    $total_pages = ceil($total_rows / $no_of_records_per_page);
                    //
                    $query = "SELECT * FROM category LEFT JOIN pictures_cat ON pictures_cat.category_id = category.id
                LEFT JOIN pictures ON pictures.id = pictures_cat.category_id";

                    $result = mysqli_query($link, $query);
                    //
                    if ($category == false || $category == 0 || $category == 8){
                        $query = "SELECT * FROM pictures LEFT JOIN pictures_cat ON pictures.id = pictures_cat.picture_id
                LEFT JOIN category ON category.id = pictures_cat.category_id
                LEFT JOIN users ON users.id = pictures.user_id WHERE pictures.user_id = $userId LIMIT $offset, $no_of_records_per_page";
                        $result = mysqli_query($link, $query);
                    } else{
                        $total_pages_sql = "SELECT COUNT(*) FROM pictures LEFT JOIN pictures_cat ON pictures.id = pictures_cat.picture_id
                LEFT JOIN category ON category.id = pictures_cat.category_id
                LEFT JOIN users ON users.id = pictures.user_id WHERE category_id = $category AND user_id =  $userId";
                        $result = mysqli_query($link,$total_pages_sql);
                        $total_rows = mysqli_fetch_array($result)[0];
                        $total_pages = ceil($total_rows / $no_of_records_per_page);

                        $query = "SELECT * FROM pictures LEFT JOIN pictures_cat ON pictures.id = pictures_cat.picture_id
                LEFT JOIN category ON category.id = pictures_cat.category_id
                LEFT JOIN users ON users.id = pictures.user_id WHERE category_id = $category AND user_id =  $userId LIMIT $offset, $no_of_records_per_page";
                        $result = mysqli_query($link, $query);
                    }
                    echo'<div class="fields">';
                    echo "<div class=\"ui three link cards\" style='margin-left: 8%;'>";
                    if(mysqli_num_rows($result)>0){
                        while ($row = mysqli_fetch_array($result)){
                            echo "<div class=\"ui card myGalleryItem\">";
                            echo "<div class=\"image\">";
                            echo "<img class='galleryItemImg' src='../src/uploads/".$row['real_name']."'/>";
                            echo "</div>";
                            echo "<div class=\"content\">";
                            echo "<div class=\"header\">";
                            echo $row['title'];
                            echo "</div>";
                            echo "<div class=\"meta\">";
                            echo "<span class=\"date\">";
                            echo "<span>";
                            echo $row['crated_time'];
                            echo "</span>";
                            echo "</span>";
                            echo "</div>";
                            echo "<div class=\"description\">"."Категория: ".$row['name']."</div>";
                            $thisPicID = $row['picture_id'];
                            $thisPicRealName = $row['real_name'];
                            $thisPicTitle = $row['title'];


                            ?>
                            <?php
                            ?>
                            <table>
                                <tr>
                                    <td>
                                        <form action="editPicture.php" method="POST">
                                            <a class="iconsLink" href='#' data-title="Редактировать" class='toggleRenameForm' onclick="parentNode.submit();"><i class="edit icon iconColor"></i></a>
                                            <input type="hidden" name="renamedPicID"  value="<?php echo $thisPicID?>"/>
                                        </form>
                                    </td>
                                    <td>
                                        <form action="deletePicture.php" method="POST" >
                                            <a class="iconsLink" href="#" data-title="Удалить" onclick="parentNode.submit();"><i class="trash alternate icon iconColor"></i></a>
                                            <input type="hidden" name="deletedId"  value="<?php echo $thisPicID?>"/>
                                            <input type="hidden" name="deletedRealName"  value="<?php echo $thisPicRealName?>"/>
                                        </form>
                                    </td>
                                    <td>
                                        <a class="iconsLink" href="<?php echo '../src/uploads/'.$row['real_name']?>" data-title="Скачать" download><i class="cloud download icon iconColor"></i></a>
                                    </td>
                                    <td>
                                        <a class="iconsLink" href="<?php echo '../src/uploads/'.$row['real_name']?>" data-title="Открыть в полном размере"><i class="expand icon iconColor"></i></a>
                                    </td>
                                </tr>
                            </table>
                            <?php
                            echo "</div>";
                            echo "</div>";
                        }
                    }

                    echo'</div>';
                    echo '</div>';

                    mysqli_free_result($result);
                    //
                    ?>
                    <div class="ui pagination menu center" style="margin-top: 30px; margin-left: 39%">
                        <a class="item" href="<?php echo "?categorySelect=".$category. '&page=1'?>">First</a>
                        <a class="item" href="<?php if($page <= 1){ echo '#'; } else { echo "?categorySelect=".$category."&page=".($page - 1); } ?>">Prev</a>
                        <a class="item" href="<?php if($page >= $total_pages){ echo '#'; } else { echo "?categorySelect=".$category."&page=".($page + 1); } ?>">Next</a>
                        <a class="item" href="<?php echo"?categorySelect=".$category. '&page='.$total_pages; ?>">Last</a>
                    </div>
                </div>
                <div class="ui bottom attached tab segment" data-tab="second">
                    <div class="addPicture">
                        <form enctype="multipart/form-data" action="addNewPicture.php" class="ui form" method="POST">
                            <div class="fields">
                                <div class="six wide field"></div>
                                <div class="four wide field">
                                    <label>Название</label>
                                    <input name="name" type="text" placeholder="Name">
                                </div>
                                <div class="six wide field"></div>
                            </div>
                            <div class="fields">
                                <div class="six wide field"></div>
                                <div class="four wide field">
                                    <label>Добавить изображение</label>
                                    <input type="hidden" name="MAX_FILE_SIZE" value="3000000" />
                                    <input name="userfile" type="file" />
                                </div>
                                <div class="six wide field"></div>
                            </div>
                            <div class="fields">
                                <div class="six wide field"></div>
                                <div class="four wide field">
                                    <label for="">Select category</label>
                                    <select class="ui fluid dropdown large" name="categorySelect">
                                        <option value="1">Flowers</option>
                                        <option value="2">Food</option>
                                        <option value="3">Sport</option>
                                        <option value="4">Cars</option>
                                        <option value="5">Ocean</option>
                                        <option value="6">Animals</option>
                                        <option value="7">Games</option>
                                    </select>
                                </div>
                                <div class="six wide field"></div>
                            </div>
                            <div class="fields">
                                <div class="sixteen wide field">
                                    <div align="center">
                                        <button class="positive ui button">Отправить</button>
                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>



                    <?php
                    $editedPicID = $_POST['renamedPicID2'];
                    $newName = $_POST['editName'];
                    $newCategory = $_POST['categorySelect'];
                    if (!empty($_POST['renamedPicID']) && !empty($newName) &&  !empty($newCategory)){
                        $query = "UPDATE pictures SET title='$newName' WHERE id = '$editedPicID'";
                        $result = mysqli_query($link, $query);
                        $query = "UPDATE pictures_cat SET category_id='$newCategory' WHERE picture_id = '$editedPicID'";
                        $result = mysqli_query($link, $query);
                    }

                    ?>
                </div>



                <?php
                require_once '../view/footer.php';
                ?>
        </div>
    </div>
</div>
</body>
<script src="/phpgallery/src/jquery-3.3.1.min.js"></script>
<script src="/phpgallery/src/Semantic-UI-CSS-master/semantic.min.js"></script>
<script src="/phpgallery/view/script.js"></script>
</html>

