<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="/phpgallery/src/Semantic-UI-CSS-master/semantic.min.css">
    <link rel="stylesheet" href="/phpgallery/view/main.css">
    <meta charset="utf-8">
    <title>gallery</title>
</head>
<body>

<div class="ui equal width aligned padded grid">
    <div class="row ">
        <div class="sixteen wide column">
            <div class="ui container">
                <div class="fields">

                <?php
                require_once '../view/header.php';
                ?>
                    <h4 class="ui horizontal divider">Registration</h4>
                <?php

                require_once ('connection.php');
//                 registration query
                if(!empty($_POST['username']) && !empty($_POST['password'])
                    && !empty($_POST['confirm_password']) && !empty($_POST['email'])
                    && $_POST['password'] == $_POST['confirm_password']
                ){


                    $username = htmlentities(mysqli_real_escape_string($link, $_POST['username']));
                    $password = htmlentities(mysqli_real_escape_string($link, $_POST['password']));
                    $passwordmd5 = md5($password);
                    $email = htmlentities(mysqli_real_escape_string($link, $_POST['email']));
                    $firstname = htmlentities(mysqli_real_escape_string($link, $_POST['firstname']));
                    $lastname = htmlentities(mysqli_real_escape_string($link, $_POST['lastname']));
                    $query ="SELECT * FROM users WHERE username = '$username'";
                    $resultUsername = mysqli_query($link, $query);
                    while ($row = mysqli_fetch_array($resultUsername)) {
                        $uniqeUsername = $row['username'];
                    }
                    $query ="SELECT * FROM users WHERE email = '$email'";
                    $resultEmail = mysqli_query($link, $query);
                    while ($row = mysqli_fetch_array($resultEmail)) {
                        $uniqeEmail = $row['email'];

                    }

                    if ($uniqeUsername == NULL) {
                        if ($uniqeEmail == NULL){
                            $query ="INSERT INTO users VALUES(NULL, '$username','$email','$firstname','$lastname','$passwordmd5')";
                            $result = mysqli_query($link, $query);
                            if($result)
                            {
                                echo "<span style='color:blue;'>Данные добавлены</span>";
                                header('Location: '.'regSucsess.php');
                            }
                        }
                    }

                };

//                 error messages
                if(isset($_POST['submit'])){
                    echo '<div class="ui error message" style="margin-bottom: 15px">';
                    echo '<ul class="list listStyle-none">';
                    if (empty($_POST['username'])){
                        echo '<li>Введите имя пользователя</li>';
                    }
                    if (empty($_POST['email'])){
                        echo '<li>Введите e-mail</li>';
                    }
                    if (empty($_POST['password'])){
                        echo '<li>Введите пароль</li>';
                    }
                    if (empty($_POST['confirm_password'])){
                        echo '<li>Введите подтверждение пароля</li>';
                    }
                    if ((isset($_POST['password'])) && (isset($_POST['confirm_password']) && $_POST['password'] !== $_POST['confirm_password'])){
                        echo '<li>Пароли должны совпадать</li>';
                    }
                    if ($uniqeUsername !== NULL) {
                    echo '<li>Такой username уже существует</li>';
                    }
                    if ($uniqeEmail !== NULL) {
                    echo '<li>Такой e-mail уже существует</li>';
                    }

                    echo '</ul>';
                    echo '</div>';
                    echo '</div>';
                };
                ?>

                <div class="regForm" id="registerForm">
                    <form action="" class="ui form" method="POST">
                        <div class="fields">
                            <div class="six wide field"></div>
                            <div class="four wide field required">
                                <label>Username</label>
                                <input name="username" type="text" placeholder="username">
                            </div>
                            <div class="six wide field"></div>
                        </div>
                        <div class="fields">
                            <div class="six wide field"></div>
                            <div class="four wide field required">
                                <label>e-mail</label>
                                <input name="email" type="text" placeholder="e-mail">
                                <div class="six wide field"></div>
                            </div>
                        </div>
                        <div class="fields">
                            <div class="six wide field"></div>
                            <div class="four wide field required">
                                <label>Password</label>
                                <input name="password" type="password" placeholder="password">
                                <div class="six wide field"></div>
                            </div>
                        </div>
                        <div class="fields">
                            <div class="six wide field"></div>
                            <div class="four wide field required">
                                <label>Confirm password</label>
                                <input name="confirm_password" type="password" placeholder="confirm password">
                                <div class="six wide field"></div>
                            </div>
                        </div>
                        <div class="fields">
                            <div class="six wide field"></div>
                            <div class="four wide field">
                                <label>First name</label>
                                <input name="firstname" type="text" placeholder="First name">
                                <div class="six wide field"></div>
                            </div>
                        </div>
                        <div class="fields">
                            <div class="six wide field"></div>
                            <div class="four wide field">
                                <label>Last name</label>
                                <input name="lastname" type="text" placeholder="Last name">
                                <div class="six wide field"></div>
                            </div>
                        </div>
                        <div class="fields">
                            <div class="sixteen wide field">
                                <div align="center">
                                    <button type="submit" name="submit" value="submit" class="positive ui button">Login</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <?php
                require_once '../view/footer.php';
                ?>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="/phpgallery/src/jquery-3.3.1.min.js"></script>
<script src="/phpgallery/src/Semantic-UI-CSS-master/semantic.min.js"></script>
<script src="/phpgallery/view/script.js"></script>
</body>
</html>


