<?php
session_start();
require_once ('connection.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="/phpgallery/src/Semantic-UI-CSS-master/semantic.min.css">
    <link rel="stylesheet" href="/phpgallery/view/main.css">
    <meta charset="utf-8">
    <title>gallery</title>
</head>
<body>
<div class="ui equal width center aligned padded grid">
    <div class="row ">
        <div class="sixteen wide column">
            <div class="ui container">
                <div class="fields">

                    <?php
                    require_once '../view/header.php';
                    $picId = $_POST['renamedPicID'];
                    ?>
                    <h4 class="ui horizontal divider editPictureInfo">Редактировать</h4>
                    <?php

                    $query = "SELECT * FROM pictures LEFT JOIN pictures_cat ON pictures.id = pictures_cat.picture_id
                LEFT JOIN category ON category.id = pictures_cat.category_id
                LEFT JOIN users ON users.id = pictures.user_id WHERE pictures.id = $picId";
                    $result = mysqli_query($link, $query);


                    while ($row = mysqli_fetch_array($result)){
                        echo "<div class=\"ui card myGalleryItem\">";
                        echo "<div class=\"image\">";
                        echo "<img class='galleryItemImg' src='../src/uploads/".$row['real_name']."'/>";
                        echo "</div>";
                        echo "<div class=\"content\">";
                        echo "<div class=\"header\">";
                        echo $row['title'];
                        echo "</div>";
                        echo "<div class=\"meta\">";
                        echo "<span class=\"date\">";
                        echo "<span>";
                        echo $row['crated_time'];
                        echo "</span>";
                        echo "</span>";
                        echo "</div>";
                        echo "<div class=\"description\">"."Категория: ".$row['name']."</div>";
                        $thisPicID = $row['picture_id'];
                        $thisPicRealName = $row['real_name'];
                        $thisPicTitle = $row['title'];

                        echo "</div>";
                        echo "</div>";
                    }



                    ?>

                    <form action="editPictureFunction.php" class="ui form" method="POST" style="padding-bottom: 80px">
                        <div class="fields">
                            <div class="six wide field"></div>
                            <div class="four wide field">
                                <label>Название</label>
                                <input name="editName" type="text" placeholder="editName" value="">
                                <input type="hidden" name="renamedPicID1" value="<?php echo $picId ?>">
                            </div>
                            <div class="six wide field"></div>
                        </div>
                        <div class="fields">
                            <div class="six wide field"></div>
                            <div class="four wide field">
                                <label for="">Select category</label>
                                <select class="ui fluid dropdown large" name="categorySelect">
                                    <option value="1">Flowers</option>
                                    <option value="2">Food</option>
                                    <option value="3">Sport</option>
                                    <option value="4">Cars</option>
                                    <option value="5">Ocean</option>
                                    <option value="6">Animals</option>
                                    <option value="7">Games</option>
                                </select>
                            </div>
                            <div class="six wide field"></div>
                        </div>
                        <div class="fields">
                            <div class="sixteen wide field">
                                <div align="center">
                                    <a href="personalGallery.php" class="ui button blue">Отмена</a>
                                    <button class="positive ui button">Применить</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <p></p>

                    <?php

                    $id = $_POST['renamedPicID1'];
                    $newName = $_POST['editName'];
                    $newCategory = $_POST['categorySelect'];
                    if (!empty($_POST['renamedPicID1']) && !empty($_POST['editName']) && !empty($_POST['categorySelect'])){
                        $query = "UPDATE pictures SET title='$newName' WHERE id = $id";
                        $result = mysqli_query($link, $query);
                        $query = "UPDATE pictures_cat SET category_id='$newCategory' WHERE picture_id = '$id'";
                        $result = mysqli_query($link, $query);
                        echo 'ok';

                    }


                    require_once '../view/footer.php';
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php

?>
</body>
<script src="/phpgallery/src/jquery-3.3.1.min.js"></script>
<script src="/phpgallery/src/Semantic-UI-CSS-master/semantic.min.js"></script>
<script src="/phpgallery/view/script.js"></script>
</html>
