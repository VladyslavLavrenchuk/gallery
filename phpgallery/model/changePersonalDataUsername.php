<?php
session_start();
require_once 'auth.php'
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="/phpgallery/src/Semantic-UI-CSS-master/semantic.min.css">
    <link rel="stylesheet" href="/phpgallery/view/main.css">
    <meta charset="utf-8">
    <title>gallery</title>
</head>
<body>
<div class="ui grid">
    <div class="row ">
        <div class="sixteen wide column">
            <div class="ui container">
                <div class="fields">
                    <?php
                    require_once '../view/header.php';
                    ?>
                    <?php
                    require_once ('connection.php');
                    $id = $_SESSION['id'];
                    $username = htmlentities(mysqli_real_escape_string($link, $_POST['username']));
                    $password = htmlentities(mysqli_real_escape_string($link, $_POST['password']));
                    $password2 = htmlentities(mysqli_real_escape_string($link, $_POST['confirm_password']));
                    $passwordmd5 = md5($password);
                    $query = "SELECT * FROM users  WHERE id = '$id'";
                    $result = mysqli_query($link, $query);
                    $row = mysqli_fetch_array($result);
                    $thisPassword = $row['password'];
                    $thisPasswordmd5 = md5($thisPassword);
                    echo '</div>';


                if(isset($_POST['submit'])){
                    if (empty($_POST['password'])){
                        echo '<div class="ui error message">';
                        echo '<div>Введите пароль</div>';
                        echo '</div>';
                    }
                    if (empty($_POST['confirm_password'])){
                        echo '<div class="ui error message">';
                        echo '<div>Введите подтверждение пароля</div>';
                        echo '</div>';

                    }
                    if (!empty($_POST['password']) && (!empty($_POST['confirm_password']) && ($_POST['password'] !== $_POST['confirm_password']))){
                        echo '<div class="ui error message">';
                        echo '<div>Пароли должны совпадать</div>';
                        echo '</div>';
                    }
                    if (empty($_POST['username'])){
                        echo '<div class="ui error message">';
                        echo '<div>Введите новое имя пользователя</div>';
                        echo '</div>';
                    }
                }



                if (!empty($_POST['username']) && $thisPassword == $passwordmd5 && $_POST['password']== $_POST['confirm_password']) {
                    $query = "UPDATE users SET username='$username' WHERE id = '$id'";
                    $result = mysqli_query($link, $query);
                    echo '<div class="ui info message">';
                    echo '<a href="../index.php" class="close icon"><i class="close icon"></i></a>';
                    echo '<div>Имя пользователя успешно изменено. Войдите в вашу учетную запись используя новые данные</div>';
                    echo '</div>';
                    session_destroy();
                }?>

                                    <form action="" class="ui form" method="POST">
                                        <div class="fields">
                                            <div class="six wide field"></div>
                                            <div class="four wide field">
                                                <label>Username</label>
                                                <input name="username" type="text" placeholder="Новое имя пользователя">
                                            </div>
                                            <div class="six wide field"></div>
                                        </div>
                                        <div class="fields">
                                            <div class="six wide field"></div>
                                            <div class="four wide field">
                                                <label>Password</label>
                                                <input name="password" type="password" placeholder="password">
                                                <div class="six wide field"></div>
                                            </div>
                                        </div>
                                        <div class="fields">
                                            <div class="six wide field"></div>
                                            <div class="four wide field">
                                                <label>Confirm password</label>
                                                <input name="confirm_password" type="password" placeholder="confirm password">
                                                <div class="six wide field"></div>
                                            </div>
                                        </div>
                                        <div class="fields">
                                            <div class="sixteen wide field">
                                                <div align="center">
                                                    <button type="submit" name="submit" value="submit" class="positive ui button">Apply changes</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <?php
                require_once '../view/footer.php';
                ?>
            </div>
        </div>
    </div>
</div>
</div>
</body>
<script src="/phpgallery/src/jquery-3.3.1.min.js"></script>
<script src="/phpgallery/src/Semantic-UI-CSS-master/semantic.min.js"></script>
<script src="/phpgallery/view/script.js"></script>
</html>


